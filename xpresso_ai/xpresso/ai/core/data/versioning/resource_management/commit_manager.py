from datetime import datetime

from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.data.versioning.resource_management.branch_manager \
    import BranchManager
from xpresso.ai.core.data.versioning.xpr_dv_commit \
    import XprDataVersioningCommit
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields
from xpresso.ai.core.data.versioning.xpr_dv_branch \
    import XprDataVersioningBranch
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import BranchInfoException


class CommitManager(PachydermResource):
    """
    Manages pachyderm commits
    """
    COMMIT_INFO_BRANCH = "branch_name"

    def __init__(self, pachyderm_client):
        super().__init__()
        self.pachyderm_client = pachyderm_client
        self.branch_manager = BranchManager(self.pachyderm_client)
        # string format for datetime object while sending in output
        self.output_datetime_format = "%A, %B %d %Y, %T"

    def inspect(self, repo_name, commit_id):
        """
        fetches commit info from pachyderm cluster

        :param repo_name:
            name of the repo commit belongs to
        :param commit_id:
            id of the commit
        :return:
            dict with commit information
        """
        commit_info = self.pachyderm_client.inspect_commit(repo_name, commit_id)
        return self.filter_commit_info(commit_info)

    def list(self, branch_object: XprDataVersioningBranch):
        """
        lists commits in a repo

        Args:
            branch_object: Instance of XprDataVersioningBranch with the info
            to filter out the commit
        Returns:
            list of commits
        """
        commit_list = self.branch_manager.list(
            branch_object, {"_id": False}
        )
        if not len(commit_list):
            # Since all record in db follow standard format,
            # if the output list is empty that implies there is no record
            raise BranchInfoException("No branch found with this info")
        elif not len(commit_list[0][DVFields.BRANCH_COMMITS_KEY.value]):
            raise BranchInfoException("No commits found for this branch")
        filtered_commit_info = self.filter_output(
            commit_list[0][DVFields.BRANCH_COMMITS_KEY.value])
        return filtered_commit_info

    @staticmethod
    def filter_commit_info(commit_info_object):
        """
        takes CommitInfo object and outputs a user friendly output

        :param commit_info_object:
            CommitInfo object
        :return:
            commit information as a dict
        """
        commit_info = dict()
        commit_info["commit_id"] = commit_info_object.commit.id if commit_info_object.commit.id else '-'
        commit_info["repo_name"] = commit_info_object.commit.repo.name
        commit_info["description"] = commit_info_object.description
        commit_info["branch_name"] = commit_info_object.branch.name
        commit_info["started_at"] = datetime.utcfromtimestamp(
            commit_info_object.started.seconds).strftime("%A, %B %d %Y, %T")
        commit_info["finished_at"] = datetime.utcfromtimestamp(
            commit_info_object.finished.seconds).strftime("%A, %B %d %Y, %T")
        # TODO: Add Start and End time in next update
        return commit_info

    def create(self, branch_object: XprDataVersioningBranch,
               commit_object: XprDataVersioningCommit):
        """
        Add the new commit into the branches collection
        """
        # Last_commit_on & Branch_created_on is required
        # in filter_output method of branch_manager
        branch_info = self.branch_manager.list(
            branch_object,
            {
                DVFields.LAST_COMMIT_ID.value: True,
                DVFields.TOTAL_FIlES_IN_BRANCH.value: True,
                DVFields.SIZE_OF_BRANCH.value: True,
                DVFields.LAST_COMMIT_ON.value: True,
                DVFields.BRANCH_CREATED_ON.value: True
            }
        )
        commit_object.set(
            DVFields.COMMITTED_BY.value,
            commit_object.get("request_uid")
        )
        new_xpresso_commit_id = \
            branch_info[0][DVFields.LAST_COMMIT_ID.value] + 1
        commit_object.set(
            DVFields.XPRESSO_COMMIT_ID.value,
            new_xpresso_commit_id
        )
        branch_object.set(
            DVFields.TOTAL_FIlES_IN_BRANCH.value,
            branch_info[0][DVFields.TOTAL_FIlES_IN_BRANCH.value]
        )
        branch_object.set(
            DVFields.SIZE_OF_BRANCH.value,
            branch_info[0][DVFields.SIZE_OF_BRANCH.value]
        )
        commit_record = commit_object.fetch_info_for_db_record()
        self.branch_manager.add_commit_to_branch(branch_object, commit_record)
        return new_xpresso_commit_id

    def filter_output(self, commit_list: list):
        """
        filter the output response from database before sending to api

        Args:
            commit_list: list pf commits as a dictionary
        Returns:
            returns updated commit info
        """
        for commit_info in commit_list:
            commit_info["commit_id"] = commit_info[DVFields.XPRESSO_COMMIT_ID.value]
            commit_info["started_at"] = commit_info["finished_at"] = \
                commit_info[DVFields.COMMITTED_ON.value]
        return commit_list
